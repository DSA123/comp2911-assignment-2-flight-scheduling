import java.util.List;
/**
 * @author David Dimitriadis
 * This class is responsible for implementing the Heuristic interface (see Heuristic.java documentation for more details),
 * as well as providing a strategy pattern to be used within the A*Search inside FlightScheduler.java 
 * (see FlightScheduler.java documentation for more details)
 */
public class FlightsLeftHeuristic implements Heuristic {
	/**
	* Calculates the heuristic path cost for a given State.
	* @precondition flightsRequired != null
	* @precondition current != null
	* @param current The State whose heuristic is being calculated.
	* @param flightsRequired The list of Flights required to be made.
	* @return An integer value for the heuristic based on what required Flights are left to make.
	*/
	@Override
	public int getPathCostH(State current, List<Flight> flightsRequired) {
		int pathCost = 0;
		for (Flight a: flightsRequired) {
			boolean foundFlight = false;
			for (Flight b: current.getRequired(current.getFlightList())) {
				if (a.doesEqual(b)) {
					foundFlight = true;
					break;
				}
			}
			if (foundFlight == false) pathCost = pathCost + a.getFlightCost();
		}
		return pathCost;
	}

}
