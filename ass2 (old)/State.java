import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author David Dimitriadis
 * This class is responsible for storing the States used within A*Search, each of which contains the current city name,
 * a list of Flights taken, as well as various path costs associated with the path taken to get to the State. This class also
 * implements Comparable<State> which is utilized within FlightScheduler for the PriorityQueue.
 */
public class State implements Comparable<State> {
	private String currentCity;
	private List<Flight> flightList;
	private int pathCostG;
	private int pathCostF;
	private int pathCostH;
public State () {
	this.flightList = new LinkedList<Flight>();
	this.pathCostG = 0;
	this.pathCostF = 0;
	this.pathCostH = 0;
}

/**
* Takes in two city names (from and to), a flight cost (the path cost between them) and a delay cost associated with the city "to".
* @precondition flightFrom != null
* @precondition flightTo != null
* @precondition flightCost >= 0
* @precondition delayCost >= 0
* @param flightFrom The name of the city where the Flight starts.
* @param flightTo The name of the city where the Flight ends.
* @param flightCost The path cost associated with the Flight.
* @param delayCost The delay cost associated with the city where the Flight ends.
* @exception  FileNotFoundException If there is no file found
*/
public void addFlight (String flightFrom, String flightTo, int flightCost, int delayCost) {
	Flight newFlight = new Flight(flightFrom, flightTo, flightCost, delayCost);
	flightList.add(newFlight);
}

/**
* Takes in a List of Flight's in order to extract the required Flights.
* @precondition requiredFlights != null
* @param requiredFlights The List of Flights whose required Flights are being extracted.
* @return A list of Flights containing all the required Flights extracted.
*/
public List<Flight> getRequired (List<Flight> requiredFlights) {
	List<Flight> requiredList = new LinkedList<Flight>();
	for (Flight a: requiredFlights) {
		for (Flight b: this.getFlightList()) {
			if (a.doesEqual(b)) {
				Flight foundFlight = new Flight(a.getFlightFrom(), a.getFlightTo(), 0, 0);
				requiredList.add(foundFlight);
				break;
			}
		}
	}
	return requiredList;
}

/**
* Compares the path costs of two States for priority within a priority queue.
* @precondition path cost >= 0
* @param o The State to be compared with the current State inside the Priority Queue.
* @return A negative integer, zero or a positive integer if the first State is less than, equal to or greater than the second respectively.
*/
public int compareTo(State o) {
	return this.getPathCostF() - o.getPathCostF();
}

public String getCurrentCity() {
	return currentCity;
}

public void setCurrentCity(String currentCity) {
	this.currentCity = currentCity;
}

public List<Flight> getFlightList() {
	return flightList;
}

public void setFlightList(List<Flight> flightList) {
	this.flightList = flightList;
}

public int getPathCostG() {
	return pathCostG;
}

public void setPathCostG(int pathCostG) {
	this.pathCostG = pathCostG;
}

public int getPathCostF() {
	return pathCostF;
}

public void setPathCostF(int pathCostF) {
	this.pathCostF = pathCostF;
}

public int getPathCostH() {
	return pathCostH;
}

public void setPathCostH(int pathCostH) {
	this.pathCostH = pathCostH;
}
}
