import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
/**
 * @author David Dimitriadis
 * This class is responsible for storing the delay cost for a city, the path cost for a flight (edge cost), 
 * implementing the A*Search algorithm with different heuristics (search strategies) as well as reading input
 * from a text file.
 * Note: The runtime analysis of the heuristic function: The sample input was tested on a range of restricted heap memory
 * through the -Xmx command within the run time configurations set within Eclipse (e.g. -Xmx16m). This input successfully ran
 * for -Xmx16m, -Xmx8m, -Xmx4m and -Xmx2m! Time taken for input.txt and input2.txt were 199883000 and 213525000 nanoseconds respectively.
*/
public class FlightScheduler {
	private Heuristic heuristic;
	private List<Flight> delayCosts;
	private List<Flight> edgeCosts;
	private List<Flight> flightsRequired;
	private int pathLength;
	private int nodesExpanded;
public FlightScheduler(Heuristic heuristic) {
	this.delayCosts = new LinkedList<Flight>();
	this.edgeCosts = new LinkedList<Flight>();
	this.flightsRequired = new LinkedList<Flight>();
	this.pathLength = 0;
	this.nodesExpanded = 0;
	this.heuristic = heuristic;
}

/**
* Takes in an input file, uses a scanner to extract the relevant data and passes it
* into the FlightScheduler to fulfill request before completing a search via A*Search.
*
* @param args The file being passed in as input
* @exception  FileNotFoundException If there is no file found
*/
public static void main(String[] args) throws FileNotFoundException{
	Scanner scan = null;
	//Heuristic heuristic = new ZeroHeuristic();
	Heuristic heuristic = new FlightsLeftHeuristic();
	FlightScheduler aStarSearch = new FlightScheduler(heuristic);
	try {
		scan = new Scanner(new FileReader(args[0]));
		while(scan.hasNext()) {
			String firstWord = scan.next();
			if (firstWord.equals("City")) {
				String cityName = scan.next();
				int delayCost = scan.nextInt();
				aStarSearch.addCity(cityName, delayCost);
			} else if (firstWord.equals("Time")) {
				String cityFrom = scan.next();
				String cityTo = scan.next();
				int flightCost = scan.nextInt();
				aStarSearch.addFlight(cityFrom, cityTo, flightCost);
			} else if (firstWord.equals("Flight")) {
				String cityFrom = scan.next();
				String cityTo = scan.next();
				aStarSearch.addReqFlights(cityFrom, cityTo);
			}
		}
		State goalState = new State();
		goalState.setFlightList(aStarSearch.getFlightsRequired());
		State initialState = new State();
		initialState.setCurrentCity("Sydney");
		State searchResult = aStarSearch.aStarSearchAlgorithm(initialState, goalState);
		System.out.println(aStarSearch.nodesExpanded + " nodes expanded");
		System.out.println("cost = " + aStarSearch.pathLength);
		for (Flight completedFlights: searchResult.getFlightList()) {
			System.out.println("Flight " + completedFlights.getFlightFrom() + " to " + completedFlights.getFlightTo());
		}
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} finally {
		if (scan != null) { scan.close(); }
	}
}

/**
* Takes in a city name and a delay cost associated with that city. Creates a new Flight with that relevant information,
* in order to store the city and its delay.
* @precondition delayCost >= 0
* @param cityName The name of the city for which the Flight will be created.
* @param delayCost The delay associated with the city.
*/
private void addCity (String cityName, int delayCost) {
	boolean cityExists = false;
	if (this.delayCosts != null) {
		for (Flight a: this.delayCosts) {
			if (a.getFlightTo().equals(cityName)) {
				cityExists = true;
				break;
			}
		}
	}
	if (!cityExists) {
		Flight newCity = new Flight(null, cityName, 0, delayCost);
		delayCosts.add(newCity);
	}
}

/**
* Takes in two city names (from and to), creates a flight between them and sets the path cost for the flight.
* @precondition flightCost >= 0
* @param flightFrom The starting city for the flight.
* @param flightTo The ending city for the flight.
* @param flightCost The path cost associated with the flight.
*/
private void addFlight (String flightFrom, String flightTo, int flightCost) {
	boolean flightExists = false;
	if (this.edgeCosts != null) {
		for (Flight addFlight: this.edgeCosts) {
			if ((addFlight.getFlightFrom().equals(flightFrom)) && (addFlight.getFlightTo().equals(flightTo))) {
				for (Flight checkFlight: this.edgeCosts) {
					if ((checkFlight.getFlightFrom().equals(flightTo)) && (checkFlight.getFlightTo().equals(flightFrom))) {
						flightExists = true;
					}
				}
				if (!flightExists) {
					Flight newFlight = new Flight(flightTo, flightFrom, flightCost, 0);
					edgeCosts.add(newFlight);
					flightExists = true;
					break;
				}
			} else if ((addFlight.getFlightFrom().equals(flightTo)) && (addFlight.getFlightTo().equals(flightFrom))) {
				Flight newFlight = new Flight(flightFrom, flightTo, flightCost, 0);
				edgeCosts.add(newFlight);
				flightExists = true;
				break;
			}
		}
	}
	if (!flightExists) {
		Flight newFlight = new Flight(flightFrom, flightTo, flightCost, 0);
		edgeCosts.add(newFlight);
		Flight newFlightTwo = new Flight(flightTo, flightFrom, flightCost, 0);
		edgeCosts.add(newFlightTwo);
	}
}

/**
* Takes in two city names (from and to) and creates a required Flight between them.
* @param flightFrom The starting city for the flight.
* @param flightTo The ending city for the flight.
*/
private void addReqFlights (String flightFrom, String flightTo) {
	boolean flightReqExists = false;
	if (this.flightsRequired != null) {
		for (Flight addReqFlight: this.flightsRequired) {
			if ((addReqFlight.getFlightFrom().equals(flightFrom)) && (addReqFlight.getFlightTo().equals(flightTo))) {
				flightReqExists = true;
				break;
			}
		}
	}
	if (!flightReqExists) {
		Flight newFlight = new Flight(flightFrom, flightTo, 0, 0);
		for (Flight a: this.edgeCosts) {
			if (a.doesEqual(newFlight)) {
				newFlight.setFlightCost(a.getFlightCost());
			}
		}
		flightsRequired.add(newFlight);
	}
}

/**
* Takes in two city names (from and to), creates a flight between them and sets the path cost for the flight.
* @precondition initialState != null
* @precondition goalState != null
* @param initialState The starting State for the search.
* @param goalState The ending State for the search.
* @return A State if the goal State is reached, otherwise null.
*/
private State aStarSearchAlgorithm(State initialState, State goalState) {
	List<State> visitedList = new LinkedList<State>();
	PriorityQueue<State> priorityQueue = new PriorityQueue<State>();
	priorityQueue.add(initialState);
	while (priorityQueue.size() > 0) {
		State currentState = priorityQueue.poll();
		this.incrementNodesExpanded();
		if (isGoal(currentState, goalState)) {
			int delay = eliminateDelay(currentState);
			this.setPathLength(currentState.getPathCostG() - delay);
			return currentState;
		}
		if (containsState(visitedList, currentState)) continue;
		for (State childState: this.getNeighbours(currentState)) {
			if (startsRequired(childState) || (finishesRequired(childState))) {
			priorityQueue.add(childState);
			}
		}
		visitedList.add(currentState);
	}
	return null;
}

/**
* Takes in a State and finds its neighboring States.
* @precondition currentState != null
* @param currentState The State whose neighbors are retrieved.
* @return A list of State's containing all neighboring States.
*/
private List<State> getNeighbours(State currentState) {
	List<State> neighbourList = new LinkedList<State>();
	for (Flight a: this.edgeCosts) {
		if (a.getFlightFrom().equals(currentState.getCurrentCity())) {
			State neighbourState = new State();
			neighbourState.setCurrentCity(a.getFlightTo());
			int delayCost = 0;
			for (Flight b: this.delayCosts) {
				if (b.getFlightTo().equals(neighbourState.getCurrentCity())) {
					delayCost = b.getDelayCost();
				}
			}
			neighbourState.setPathCostH(this.heuristic.getPathCostH(neighbourState, this.flightsRequired));
			neighbourState.setPathCostG(currentState.getPathCostG() + a.getFlightCost() + delayCost);
			neighbourState.setFlightList(copyFlights(currentState));
			neighbourState.addFlight(currentState.getCurrentCity(), neighbourState.getCurrentCity(), a.getFlightCost(), delayCost);
			neighbourState.setPathCostF(neighbourState.getPathCostG() + neighbourState.getPathCostH());
			neighbourList.add(neighbourState);
		}
	}
	return neighbourList;
}

/**
* Takes in a State and copies its Flights into a new list. 
* @precondition currentState != null
* @param currentState The State whose Flights are copied.
* @return A new list of Flights containing all Flights contained within the State given.
*/
private List<Flight> copyFlights (State currentState) {
	List<Flight> copiedFlights = new LinkedList<Flight>();
	copiedFlights.addAll(currentState.getFlightList());
	return copiedFlights;
}

/**
* Takes in a State and calculates its current cities delay in order to eliminate it from the path cost at the end of the A*Search.
* @precondition currentState != null
* @param currentState The State whose delay is calculated.
* @return An integer that contains the delay cost.
*/
private int eliminateDelay (State currentState) {
	int delayCost = 0;
	for (Flight a: this.delayCosts) {
		if (a.getFlightTo().equals(currentState.getCurrentCity())) {
			delayCost = a.getDelayCost();
		}
	}
	return delayCost;
}

/**
* Takes in two States and determines whether the first contains the same required flights as the second.
* @precondition current != null
* @precondition goal != null
* @param current The State which is being compared with the goal State.
* @param goal The State containing all the required flights.
* @return True if current contains the necessary Flights, otherwise False.
*/
private boolean isGoal (State current, State goal) {
	boolean isGoal = false;
	int flightsFound = 0;
	for (Flight flightList: goal.getFlightList()) {
		for (Flight flightListTwo: current.getFlightList()) {
			if (flightList.doesEqual(flightListTwo)) {
			flightsFound++;
			break;
			}
		}	
	}
	if (flightsFound == goal.getFlightList().size()) isGoal = true;
	return isGoal;
}

/**
* Takes in a list of States and a State in order to determine whether the list contains a State which has the same required flights as the State given.
* @precondition containingList != null
* @precondition checkedState != null
* @param containingList The list of States which is being compared with the given State.
* @param checkedState The State that is compared with the States in the list.
* @return True if containingList contains a State with the necessary Flights, otherwise False.
*/
private boolean containsState (List<State> containingList, State checkedState) {
	boolean doesContain = false;
	for (State a : containingList){
		if (a.getCurrentCity().equals(checkedState.getCurrentCity())) {		
			if(a.isSameState(checkedState, flightsRequired)){
				doesContain = true;;
			}
		}
	}
	return doesContain;
}

/**
* Takes in a State in order to determine whether the State begins a required flight.
* @precondition current != null
* @param current The State being checked for starting city.
* @return True if the State's current City begins a required flight.
*/
private boolean startsRequired(State current){
	boolean preparesFlight = false;  
	for (Flight a : this.flightsRequired){
	   if (a.getFlightFrom().equals(current.getCurrentCity())){
	    preparesFlight = true;
	   }
	}
	return preparesFlight;
}

/**
* Takes in a State in order to determine whether the State finishes a required flight.
* @precondition current != null
* @param current The State being checked for finishing a required flight.
* @return True if the State's last flight is a required one.
*/
private boolean finishesRequired(State current) {
	boolean flightFound = false;
	int listSize = current.getFlightList().size();
	Flight flightCompleted;
	if (listSize == 0) {
		flightCompleted = current.getFlightList().get(0);
	} else {
		flightCompleted = current.getFlightList().get(listSize - 1);
	}
	for (Flight a: this.flightsRequired) {
		if (a.doesEqual(flightCompleted)) {
			flightFound = true;
		}
	}
	return flightFound;
}

/**
* Increments the number of nodes expanded in order to track the total.
*/
private void incrementNodesExpanded() {
	this.nodesExpanded++;
}

private void setPathLength(int pathLength) {
	this.pathLength = pathLength;
}

private List<Flight> getFlightsRequired() {
	return flightsRequired;
}
}
