import java.util.List;

/**
 * @author David Dimitriadis
 * This class is responsible for implementing the Heuristic interface (see Heuristic.java documentation for more details),
 * as well as providing a strategy pattern to be used within the A*Search inside FlightScheduler.java 
 * (see FlightScheduler.java documentation for more details)
 */
public class ZeroHeuristic implements Heuristic {
	@Override
	/**
	* Calculates the heuristic path cost for a given State.
	* @precondition flightsRequired != null
	* @precondition current != null
	* @param current The State whose heuristic is being calculated.
	* @param flightsRequired The list of Flights required to be made.
	* @return An integer value for the heuristic, in this case, 0.
	*/
	public int getPathCostH(State current, List<Flight> flightsRequired) {
		return 0;
	}
}
