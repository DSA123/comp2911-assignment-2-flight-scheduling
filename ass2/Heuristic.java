import java.util.List;

/**
 * @author David Dimitriadis
 * This class is responsible for the interface of the strategy pattern utilized within the A*Search inside FlightScheduler.java
 * (see FlightScheduler.java documentation for more details).
 */
public interface Heuristic {
	/**
	* Calculates the heuristic path cost for a given State.
	* @precondition flightsRequired != null
	* @precondition current != null
	* @param current The State whose heuristic is being calculated.
	* @param flightsRequired The list of Flights required to be made.
	* @return An integer value for the heuristic.
	*/
	public int getPathCostH(State current, List<Flight> flightsRequired);
}
