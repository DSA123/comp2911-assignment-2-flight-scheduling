/**
 * @author David Dimitriadis
 * This class is responsible for storing the information about each Flight created, 
 * each of which contains the city name associated, with the start and end of the Flight, 
 * the cost associated with the Flight and the delay cost associated with the destination city.
 */
public class Flight {
	private String flightFrom;
	private String flightTo;
	private int flightCost;
	private int delayCost;
public Flight (String flightFrom, String flightTo, int flightCost, int delayCost) {
	this.flightFrom = flightFrom;
	this.flightTo = flightTo;
	this.flightCost = flightCost;
	this.delayCost = delayCost;
}

/**
* Compares two Flights to determine if they are equal.
* @precondition comparedFlight != null
* @param comparedFlight The Flight to be compared.
* @return True if the Flights are equal, otherwise False.
*/
public boolean doesEqual (Flight comparedFlight) {
	boolean equals = false;
	if (this.getFlightFrom().equals(comparedFlight.getFlightFrom())) {
		if (this.getFlightTo().equals(comparedFlight.getFlightTo())) {
			equals = true;
		}
	}
	return equals;
}

public String getFlightFrom() {
	return flightFrom;
}

public String getFlightTo() {
	return flightTo;
}

public int getFlightCost() {
	return flightCost;
}

public void setFlightCost(int flightCost) {
	this.flightCost = flightCost;
}

public int getDelayCost() {
	return delayCost;
}
}